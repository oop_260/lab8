package com.pornchitar.week8;

public class Rectangle {
    private String name;
    private double width;
    private double height;
    private double area;
    private double perimeter;
    public Rectangle(String name, double width, double height){
        this.name = name;
        this.width = width;
        this.height = height;
    }
    public void PrintAreaRect() {
        area = width * height;
        System.out.println("Area of Rectangel " +name+ " : " + area);
    }

    public void printPerimeterRect() {
        perimeter = (width * 2) + (height * 2);
        System.out.println("Perimeter of Rectangel "+name+ " : " + perimeter);
    }
    
}
