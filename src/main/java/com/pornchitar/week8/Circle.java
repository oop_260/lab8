package com.pornchitar.week8;

public class Circle {
    private double r;
    private String name;
    private double area;
    private double perimeter;
    public Circle(String name, double r){
        this.r = r;
        this.name = name;
    }
    public void PrintAreaCircle(){
        area = Math.PI*(Math.pow(r,2));
        System.out.println("Area of Circle "+name+ " : " + area);
    }
    public void PrintPerimeterCircle(){
        perimeter = 2*(Math.PI*r);
        System.out.println("Perimeter of Circle "+name+ " : " + perimeter);
    }
}
