package com.pornchitar.week8;

public class Triangle {
    private String name;
    private double a;
    private double b;
    private double c;
    private double area;
    private double perimeter;
    public Triangle(String name, double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;
        this.name = name;
    }
    public void PrintAreaTriangle(){
        double sum = (a+b+c)/2;
        area = Math.sqrt(sum*(sum-a)*(sum-b)*(sum-c));
        System.out.println("Area of Triangle "+name+ " : " + area);
    }
    public void PrintPerimeterTriangle(){
        perimeter = a+b+c;
        System.out.println("Perimeter of Triangle "+name+ " : " + perimeter);
    }
}
