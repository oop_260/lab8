package com.pornchitar.week8;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle("rect1", 10, 5);
        rect1.PrintAreaRect();
        rect1.printPerimeterRect();
        Rectangle rect2 = new Rectangle("rect2", 5, 3);
        rect2.PrintAreaRect();
        rect2.printPerimeterRect();

        Circle circle1 = new Circle("circle1", 1);
        circle1.PrintAreaCircle();
        circle1.PrintPerimeterCircle();
        Circle circle2 = new Circle("circle2", 2);
        circle2.PrintAreaCircle();
        circle2.PrintPerimeterCircle();

        Triangle triangle = new Triangle("triangle",5, 5, 6);
        triangle.PrintAreaTriangle();
        triangle.PrintPerimeterTriangle();
    }
}
